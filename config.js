module.exports = {
  development: {
  	hostname: process.env.HOST || "0.0.0.0",
    port: process.env.PORT || 3000,
  	saltingRounds: 10
  }
}