let app = require('../app');
const chai = require("chai");
const chaiHttp = require("chai-http");

// Configure chai
chai.use(chaiHttp);
chai.should();

describe("User Functions", () => {
  // Try logging in with existing user
  describe("POST /login with existing user", () => {
	it("Try logging in with correct credentials", (done) => {
            chai.request(app)
            .post('/login')
            .set('content-type', 'application/json')
            .send({
        			username: 'testlogin',
        			pw: 'Koodaus1'
                })
            .end(function(error, res, body) {
                if (error) {
                    done(error);
                } else {
                    res.should.have.status(200);
                    done();
                }
            });
     });
  });

  // Try logging in with undefined credentials
  describe("POST /login undefined user", () => {
	it("Try logging in with undefined credentials", (done) => {
            chai.request(app)
            .post('/login')
            .set('content-type', 'application/json')
            .send({
        			username: 'testlogin',
        			pw: '12345'
                })
            .end(function(error, res, body) {
                if (error) {
                    done(error);
                } else {
                    res.should.have.status(401);
                    done();
                }
            });
     });
  });

  // Post data with garbage
  describe("POST /data with garbage", () => {
	it("Try posting data with completely unrelated data", (done) => {
            chai.request(app)
            .post('/data')
            .set('content-type', 'application/json')
            .send({
        			username: 'liibalaabaa',
        			pw: 'hölynpölyy'
                })
            .end(function(error, res, body) {
                if (error) {
                    done(error);
                } else {
                    res.should.have.status(404);
                    done();
                }
            });
     });
  });

  // Send work hours to MySQL database with test user
  describe("POST /data", () => {
	it("Sends work hours to MySQL Database with test user", (done) => {
            chai.request(app)
            .post('/data')
            .set('content-type', 'application/json')
            .send({
                    kaynnin_tyyppi: 'testType', 
        			asiakas_lkm: 13, 
        			tunnit: 1337,
        			vuosi: 2000,
        			username: 'testlogin',
        			pw: 'Koodaus1'
                })
            .end(function(error, res, body) {
                if (error) {
                    done(error);
                } else {
                    done();
                }
            });
     });
  });

  // Sends work hours to MySQL database but with a undefined user
  describe("POST /data undefined username", () => {
	it("Sends work hours to MySQL Database but with a undefined user", (done) => {
            chai.request(app)
            .post('/data')
            .set('content-type', 'application/json')
            .send({
                    kaynnin_tyyppi: 'testType', 
        			asiakas_lkm: 13, 
        			tunnit: 1337,
        			vuosi: 2000,
                })
            .end(function(error, res, body) {
                if (error) {
                    done(error);
                } else {
                    res.should.have.status(404);
                    done();
                }
            });
     });
  });

  // Sends work hours to MySQL database with a long username string
  describe("POST /data too long username", () => {
	it("Sends work hours to MySQL Database but with a long username string", (done) => {
            chai.request(app)
            .post('/data')
            .set('content-type', 'application/json')
            .send({
                    kaynnin_tyyppi: 'testType', 
        			asiakas_lkm: 13, 
        			tunnit: 1337,
        			vuosi: 2000,
            		username: 'wvreasafvweravsvdfaawevrewvasdafveawrvasdafvasdfvaefvsadfv',
        			pw: '12345'
                })
            .end(function(error, res, body) {
                if (error) {
                    done(error);
                } else {
                    res.should.have.status(404);
                    done();
                }
            });
     });
  });

  // Sends work hours to MySQL database with a undefined password
  describe("POST /data undefined password", () => {
	it("Sends work hours to MySQL Database but with a undefined password", (done) => {
            chai.request(app)
            .post('/data')
            .set('content-type', 'application/json')
            .send({
                    kaynnin_tyyppi: 'testType', 
        			asiakas_lkm: 13, 
        			tunnit: 1337,
        			vuosi: 2000,
                    username: 'testlogin'
                })
            .end(function(error, res, body) {
                if (error) {
                    done(error);
                } else {
                	res.should.have.status(404);
                    done();
                }
            });
     });
  });

  // Sends work hours to MySQL database with a long password string
  describe("POST /data too long password", () => {
	it("Sends work hours to MySQL Database but with a long password string", (done) => {
            chai.request(app)
            .post('/data')
            .set('content-type', 'application/json')
            .send({
                    kaynnin_tyyppi: 'testType', 
        			asiakas_lkm: 13, 
        			tunnit: 1337,
        			vuosi: 2000,
            		username: 'testlogin',
        			pw: 'awerwvrawrveawvrawceasdfvsaveawvfasdvafd'
                })
            .end(function(error, res, body) {
                if (error) {
                    done(error);
                } else {
                    res.should.have.status(401);
                    done();
                }
            });
     });
  });

  // Sends work hours to MySQL database with a undefined everything
  describe("POST /data undefined everything", () => {
	it("Sends work hours to MySQL Database but with undefined everything", (done) => {
            chai.request(app)
            .post('/data')
            .set('content-type', 'application/json')
            .send()
            .end(function(error, res, body) {
                if (error) {
                    done(error);
                } else {
                    res.should.have.status(404);
                    done();
                }
            });
     });
  });

  // Gets all work hours from MySQL database
  describe("GET /data", () => {
	it("Gets all work hours from MySQL Database", (done) => {
            chai.request(app)
            .get('/data')
    		.end(function(error, res, body) {
                if (error) {
                    done(error);
                } else {
                    res.should.have.status(200);
					done();
                }
            });
     });
  });
});