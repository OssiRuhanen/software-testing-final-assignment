const bcrypt = require('bcrypt');

const environment = process.env.NODE_ENV;
const stage = require('../config')[environment];

const fs   = require('fs');
const jwt  = require('jsonwebtoken');
// PRIVATE and PUBLIC key
var privateKEY  = fs.readFileSync('./private.key', 'utf8');
var publicKEY  = fs.readFileSync('./public.key', 'utf8');

var i  = 'LamkStudent';          // Issuer 
var s  = 'some@user.com';        // Subject 
var a  = 'http://kuulo.lamk.fi'; // Audience

// SIGNING OPTIONS
var signOptions = {
 issuer:  i,
 subject:  s,
 audience:  a,
 expiresIn:  "12h",
 algorithm:  "RS256"
};

var con = mysql.createConnection({
    host: "localhost",
    user: "testdatabaseuser",
    password: "123456",
    database: "dev"
});

con.connect(function(err) {
    if (err) { throw err; }

    console.log("Connected to DB");
});

class userSchema {
  	constructor(body) {
   		this.username = body.username;
  		this.password = body.password;
    	this.database = body.database;
  	}

    static findUserWithCredentials(username, password) {
        if (username !== undefined && pw !== undefined) {
            console.log("Searching for user: ", user);

            var sql = "SELECT * FROM tilit WHERE username = ?";
            con.query(sql, [ username ], function(err, rows, fields) 
            {
                if (err) {
                    console.log(err);
                    throw new Error({error: 'Invalid password or username'})
                } else 
                {
                    if (rows.length == 0) 
                    {
                        throw new Error({error: 'User does not exist!'})
                    } else 
                    {   
                        // We could compare passwords in our model instead of below
                        bcrypt.compare(password, rows[0].password).then(match => {
                            if (match) {          
                                console.log("Successfully logged in!");
                            } else {
                                throw new Error({error: 'Invalid password or username'})
                            }
                        }).catch(err => {
                            throw new Error({error: err})
                        });
                    }
                }
            });
        } else {
            res.sendStatus(statusNotFound);
        }
    }

	save() {
    	const user = this;
        bcrypt.hash(user.password, stage.saltingRounds || 10, function(err, hash) {
          if (err) {
             throw new Error({error: 'Error hashing password for user', user.password})
          } else {   
            var sql = "INSERT INTO tilit (username, password, database) VALUES (?, ?, ?)";
            console.log(`Creating new account: ${user.username} & ${hash} & ${user.database}`); 
            con.query(sql, [ user.username, hash,user.database ], function(err, rows, fields) 
            {
                if (err) {
                    throw new Error({error: 'Failed to create!'})
                }
            });
          }
    	});
    }

	generateAuthToken() {
        const user = this;
    	const token = jwt.sign({_id: user._id}, privateKEY);
    	user.tokens = user.tokens.concat({token});
    	await user.save();
    	return token;
    }

}

const User = userSchema;
module.exports = User