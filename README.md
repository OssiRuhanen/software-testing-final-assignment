# Kuulomobiiliapp

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- A Computer

### Installing

1. Clone the repository
2. Setup mysql server with the script in db\scripts\bootstrap.sh
3. npm install
4. Start the node app with npm run dev



### Test descriptions

1  Tests logging with valid credentials

- Expected output: Status 200 

2 Tests logging with wrong password

- Expected output: Status 401

3 Test logging with wrong username and password

- Expected output: Status 404

4 Tests sending work hours to MySQL Database with test user

- Expected result: pass

5 Tests sending work hours to MySQL Database with invalid credentials

- Expected output: Status 404

6 Tests sending work hours to database but with long username

- Expected output: Status 404

7 Tests sending work hours to database but with undefined password

- Expected output: Status 404

8 Tests sending work hours to database with too long password

- Expected output: Status 401

9 Tests sending work hours to database with undefined user/pw

- Expected output: Status 404

10 Tests getting all work hours from MySQL database

- Expected output: Status 200