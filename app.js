// nodejs_server/index.js
require('dotenv').config(); // Sets up dotenv as soon as our application starts

const mysql = require('mysql');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const app = express();
const bcrypt = require('bcrypt');

// const userRouter = require('./routers/user')

const environment = process.env.NODE_ENV; // development
const stage = require('./config')[environment];

if (environment !== 'production') {
      app.use(logger('dev'));
}

// bodyParser is a type of middleware
// It helps convert JSON strings
// the 'use' method assigns a middleware
app.use(bodyParser.json({ type: 'application/json' }));

app.use(
    cors({
        origin: "*", // restrict calls to those this address
        methods: "GET, POST" // only allow GET requests
    })
);

// app.use(userRouter);

// http status codes
const statusOK = 200;
const statusNotFound = 404;

var con = mysql.createConnection({
    host: "localhost",
    user: "test_user",
    password: "test_password",
    database: "dev"
});

con.connect(function(err) {
    if (err) { throw err; }

    console.log("Connected to DB");
});

app.post('/login', function(req, res) {
    if (req.body == undefined) {
        res.sendStatus(statusNotFound);
    }

    var user = req.body;
    var username = user.username;
    var pw = user.pw;

    if (username !== undefined && pw !== undefined) {
        console.log(user);

        var sql = "SELECT * FROM tilit WHERE username = ?";
        con.query(sql, [ username ], function(err, rows, fields) 
        {
            if (err) {
                console.log(err);
                res.sendStatus(statusNotFound);
            } else 
            {
                if (rows.length == 0) 
                {
                    res.sendStatus(statusNotFound);
                } else 
                {   
                    var dbPw = rows[0].password;

                    // We could compare passwords in our model instead of below
                    bcrypt.compare(pw, dbPw).then(match => {
                        if (match) {          
                            res.sendStatus(statusOK);
                        } else {
                            res.sendStatus(401);
                        }
                    }).catch(err => {
                        console.log(err);
                        res.sendStatus(500);
                    });
                }
            }
        });
    } else {
        res.sendStatus(statusNotFound);
    }
});

app.post('/users', function(req, res) {
    var user = req.body;

    var name = user.name;
    var pw = user.pw;
    var db = user.db;

    if (username !== undefined && pw !== undefined && db !== undefined) {

    	console.log(`Success`);
    	res.sendStatus(statusOK);
    
	} else {
    	console.log('Authentication error');
    	res.sendStatus(401);
    }
});

// Handle GET data request
app.get('/data', function(req, res) {
    con.query("SELECT * FROM Vapaaehtoistyotiedot", function (err, result, fields)
    {
      if (err) {
         res.sendStatus(statusNotFound);
      }

      res.statusCode = statusOK;
      res.send(result);
    });
});

// Handle POST request
app.post('/data', function(req, res) {
    if (req.body == undefined) {
        res.sendStatus(statusNotFound);
    }

    var asiakasObj = req.body; // TODO validate data

    var tyyppi = asiakasObj.kaynnin_tyyppi;
    var lkm = asiakasObj.asiakas_lkm;
    var tunnit = asiakasObj.tunnit;
    var vuosi = asiakasObj.vuosi;

    var username = asiakasObj.username;
    var pw = asiakasObj.pw;

    if (username !== undefined && pw !== undefined &&
        tyyppi !== undefined && lkm !== undefined &&
        tunnit !== undefined && vuosi !== undefined
        ) {
        console.log(asiakasObj);

        var sql = "SELECT * FROM tilit WHERE username = ?";
        con.query(sql, [ username ], function(err, rows, fields) 
        {
            console.log(`SQL Searching for user: ${username}`);
            if (err) {
                console.log("Not found");
                res.sendStatus(statusNotFound);
            } else 
            {
                console.log(rows.length);
                if (rows.length == 0) 
                {
                    console.log("Row length == 0");
                    res.sendStatus(statusNotFound);
                } else 
                {   
                    console.log(rows[0].database);
                    var dbPw = rows[0].password;

                    // We could compare passwords in our model instead of below
                    bcrypt.compare(pw, dbPw).then(match => {
                        if (match) {          
                            var sql = "INSERT INTO Vapaaehtoistyotiedot (kaynnin_tyyppi, asiakas_lkm, tunnit, vuosi) VALUES (?, ?, ?, ?)";
                            console.log(`Inserting work hours to database: ${tyyppi} & ${lkm} & ${tunnit}`);   
                            con.query(sql, [ tyyppi, lkm, tunnit, vuosi ], function(err, rows, fields) 
                            {
                                if (err) {         // send response
                                    console.log("Not found");
                                    res.sendStatus(statusNotFound);
                                }
                            });

                            console.log(`Item added with vuosi: ${vuosi}`);
                            res.sendStatus(statusOK);
                        } else {
                            console.log('Authentication error');
                            res.sendStatus(401);
                        }
                    }).catch(err => {
                        console.log(err);
                        res.sendStatus(500);
                        console.log('No access!');
                    });
                }
            }
        });
    } else {
        res.sendStatus(statusNotFound);
    }
});

app.listen(`${stage.port}`, `${stage.hostname}`, () => {
    console.warn(`Listening at http://${stage.hostname}:${stage.port}/...`);
});

module.exports = app;