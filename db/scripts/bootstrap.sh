#!/bin/bash

# Find dir script is running in
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

echo "Running base scripts"
mysql -v -h 127.0.0.1 -u test_user --password=test_password -D dev < $DIR/../base/vapaaehtoistyotiedot.sql
mysql -v -h 127.0.0.1 -u test_user --password=test_password -D dev < $DIR/../base/tilit.sql